# Governance of the Infrared Scoping and Mapping Research Project

This is a governance document that would elaborates the roles, scope, and decision making of the advisors who comprise a sub working group of Infrared.

Membership in this group is open and voluntary. It meets twice monthly to check in on the progress of the research work plan document. (Link to work plan in 0xacab)

The current members of the group have self-selected their interest. They are:
 * Mallory, equalitie (infrared member), lead researcher
 * Beatrice, independent (infrared subscriber), researcher
 * Gunner
 * Brenna, independent, infrared subscriber/former org member, researcher
 * Georgia

(Undefined: bias, challenges, framings that we bring to the work)

This research project is a funded concept written by Mallory and Beatrice and approved by the Ford Foundation and runs from January-December 2024. It implements desk research as well as qualitative and quantitative methods to map the landscape in which Infrared is and can be an actor as well as scope its activities, past, present and future. (Link to concept in 0xacab)

The research work plan is a document that elaborates the approved concept. It defines:
 * what is the research and its questions?
 * The goals of the research vis a vis Infrared. (Link to work plan in 0xacab)

Accountability modes:
 * Advisors to the research group have a Signal chat.
 * They are a group in the Infrared project on 0xacab, which gives them admin over that sub project, its documents and its open tickets 
 * They meet every two weeks
 * An agenda is sent in advance of meetings
 * Minutes are taken and kept in 0xacab (link to minutes directory in 0xacab)
 * Research documents are organized in 0xacab
 * Someone from this group will give an abbreviated update on the research project at the beginning of each Infrared meeting
 * We send periodic (monthly) updates to the email list.

Tools that we administer:

 * 0xacab: riseup
 * Nextcloud: federated by Infrared members
 * transcription and autotranslation tools: offline tools.

 # Principles for data stewardship

Non-negotiable: recording is always opt-out.

1. We use boilerplate release form that would work under GDPR, raw data and unredacted drafts only available to researchers.  
2. We ask Infrared members to consider drafting a bespoke policy. 
3. (Another option)

Data stewardship and release forms key features: 

 * Where we keep it
 * When we delete it
 * Who can see it
 * What is the data
    * Transcripts of interviews
    * Surveys