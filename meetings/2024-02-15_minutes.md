#Agenda
0. Checks in
1. Updates
2. Debrief from NPDev and last infrared call
3. Research plan and next steps
4. Housekeeping, next call

## Checks in and updates

Ford Foundation came through with funding to Mallory's entity at the amount we asked for ($50,000).

## Debrief from NPDev and last infrared call

Beatrice: Members want to be more involved.
Gunner: Super dynamic we need to think about expanding who we are.
Brenna: Opportunities this year to do that include cryptorave, global gathering, npdev 2024.

## Research plan and next steps

Current plan has two parts: 1) Needs, challenges, wishes and 2) growth. But given the debriefs we need to add in a first piece on the network itself who is in and who is out.

Propose update to research plan in three parts:
A -- who we are, who is in/out, services, capacities -- APC Member Meeting
B -- needs, challenges, wishes -- Global Gathering
C -- horizon scan, mapping opportunities and growth -- NP Dev 2024

## Housekeeping, next call

We will meet bi-monthly for one hour. Research team can stay for some or all of the meetings or send regrets as needed.

Brenna to work with mallory on the gitlab space based on the work she has done that does not yet have a space.
Mallory to ask Gunner for use of the next infrared meeting slot to launch the work and ask for input.
We meet again in two weeks, Mallory to edit the calendar invitation to repeat.

