# 29 September 2023
Action points from prior meetings, plus this meeting:

- [x] Create a project in 0xacab – Brenna
    - [x] Move over Project proposal – Brenna
- [x] Move over Research plan (perhaps only once it’s finalised in Gdocs?) – TBD
- [ ] Pull down infrared list membership and compare to the listed members in 0xacab – TBD
- [ ] Review research plan, make comments and suggestions (see “TODO” items in https://0xacab.org/infrared/members/Research/-/blob/no-masters/ResearchPlan.md – All
- Apply to:
    - Digital Infrastructure Fund – Mallory
    - Agency Fund https://www.agency.fund/apply – Georgia
    - FIRN https://www.apc.org/en/news/feminist-internet-research-network-call-research-proposals-1 – Mallory
    - OTF IF Fund – TBD
