# Research question
According to the current composition of Infrared, what are the network’s capabilities and gaps? What are the potential opportunities for its future growth?

# Extended abstract
Infrared is a network of Internet infrastructure providers from around the world that provide alternatives to multinational corporate platforms to underserved, underrepresented and at-risk communities of advocates, activists, human rights defenders and journalists and media workers.

The aim of the member organizations involved in Infrared is to share knowledge about how to most effectively and sustainably serve the communities and individuals they strive to support, thus strengthening their own operations as well as contributing to collective initiatives ultimately boosting each other’s capabilities and resources.

For this purpose, it is key for the Infrared network to:

- gather a clear and in-depth understanding of its members’ individual and collective needs and wishes
- clarify where Infrared can meet needs elsewhere by developing a map of the ecosystem it works within, constituted by several like-minded and often intersecting communities, such as RaReNet (Rapid Response Network), CiviCERT and other networks and coalitions.

This will allow the Infrared network to most strategically develop and implement its initiatives as a key actor and partner in the independent Internet infrastructure provider sector.

# Roles and responsibilities

Research lead -- Mallory

Researchers -- Mallory, Brenna, Beatrice

Advisors -- Gunner, Georgia

# Methodology

## Phase I: Development

### 1.1 Desk research 

- Conduct literature review and highlights key areas for questions 
- Go back to prior documentation from Infrared meetings
- 2017 Montreal
- 2017 SF @ NP Dev
- 2018 Valencia pre-day @ IFF
- 2018 Valencia @ Feminist gathering
- 2019 Valencia @ IFF
- 2019 SF @ NPDev
- 2019 Mozfest

### 1.2 Question Development
- Develop two sets of questions based on key takeaways from desk research: one set for an internal survey to gather basic details, and the other for interviews for more qualitative content.

### 1.3 Methodology Development
- Draft methodologies section drawn from desk research findings and similar field surveys consulted in the desk research; to be approved before phase II. 

## Phase II: Feedback and collection @ NPDev Summit

### 2.1 Identify stakeholders
- The stakeholders whose responses will be most relevant to this project are Infrared members: these groups will best be able to reflect on the capabilities, gaps and opportunities.

### 2.2. Surveys
- A survey will be sent out to stakeholders. Questions focus on 3 themes: capabilities, gaps and opportunities. Questions are quantitative and agreed up on by the Infrared members at the NPDev Summit in November 2023.
### 2.3 Interviews
- Post NPDev Summit in which we 
- Live interviews will be conducted over a digital media, video/audio chat and a survey template for notes on each question. Interview audio will be recorded and transcribed by the interviewer.
- Prior to each interview, the participant will be sent an informed consent form which explains the voluntary nature of the study and that there are no risks associated with the research. It will also state whether or not the interview is being done anonymously, who will have access to the interview data and for how long, the participant's right to stop the interview at any time or withdraw from the study later on, and CDT's contact information. The link to the (draft) informed consent form: TODO
- The list of interviewees will be identified by drawing from the Infrared subscription list-- ideally, the set of interviewees will encompass a range of experiences within the field both in terms of professional responsibilities and personal identity.

## Phase III: Synthesis
### 3.3. Synthesis
- Main task at this stage could be reviewing survey and interview responses and drawing out trends and key takeaways, then bringing that to bear on initial research questions and subquestions.
- External researcher codes interview transcripts and/or interview notes and draws out  themes using qualitative data analysis tools where useful to supplement manual coding. 

### 3.4. Preliminary analysis.
- At this stage, researchers will verify that all research questions have been answered and sketch out the main points of the narrative.

### 3.5. Stakeholder feedback.
- Project team
- Infrared members
- Funders?

### 3.6. Internal and external reports.
- Start from common base (especially desk research/overview of goals) and then differentiate for audience-- internal report could include more detail on survey results that are Infrared-specific
- Interview transcriptions and summary of survey results could be documented as appendices to the final reports?

# Timeline and activities
Milestone check-ins occur every weeks:

- Week 1: 29 September 2023
- Week 6, NPDev: 14 November 2023
- Week 10, preliminary analysis: 7 December 2023

#todo add weeks x phase milestones table

## 1.1. Desk research
**Research question:** XX

Gaps and additions will be incorporated iteratively. The desk research can be used as the basis for communicating the direct aims of this project to those participating in surveys and interviews. Portions of the desk research will also be used in the final reports.

#todo _add Desk research goes here_

## 1.2. Question development
Based on the literature review, sub-questions will be investigated either for further research or for respondents to surveys and interviews. We may want key individuals to answer both a survey and participate in an interview, so these questions should be somewhat orthogonal.

1.	What are the roles that best describe the work done by the members of your organization?
2.	Which competencies are represented in your organization?
3.	Which competencies are not represented in your organization, why this is the case, and which would you prioritize acquiring?
4.	In your opinion/ to the extent of your knowledge, which of the above are well represented in terms of capacity across the Infrared network? Which are missing and would be important to acquire?
5.	What are shared projects that your organization would find most relevant to prioritize and collaborate on, leveraging the connections built through Infrared?
6.	In your opinion, how can we ensure that the initiatives carried out across Infrared are equitable? In this regard, we welcome your input on matters of sustainability, time availability, as well as compensation and incentives models.

In the qualitative portion, we want to build on desk research and quantitative research inquiries, but as an initial start here are some interview questions:

1.	In your opinion, what are the key actors (e.g. individual or networks/coalitions/fora of organizations, practitioners, service providers) operating across the ecosystem Infrared is part of?
2.	Are there collaboration opportunities with the aforementioned actors that, in your opinion, would be most impactful for the services that the Infrared network strives to provide? If so, what are they, and how would you envision exploring them?
1.3. Methodology development
Based on the questions raised by the literature review, and in development of the questions, a methodology should become apparent. Once methodology is concrete, this proposal will be revised and signed off before beginning Phase II.
#todo add Methods description here

## 2.1. Identifying stakeholders
Who is on the Infrared list? #todo

## 2.2. Survey
Surveys will be used to control for responses across multiple respondents in Infrared for basic information and statistics to use in the research.
The questions will be put into a free software web survey. #todo

Intro text:
#todo add Survey intro text here

## 2.3. Interviews
Interviews are meant to go much further in depth than the survey and should lead to more qualitative understanding of where the network is at. Interviews will get at the heart of what the Infrared members work on and what they wish they could work on.

The interview questions should be broad enough to lead to unique and deep discussion, while small in number such that the interview doesn’t exceed 45 minutes. Getting the balance right is crucial.

### Draft Intro/email text:

Dear [ ],

#todo [Write scheduling email]

You can sign up for a slot on my calendar at this link:

#todo [Input calendar link]

If you’re unavailable for a call, we’ve also created a form that you can fill out and send back at your convenience:

#todo [Create form and insert link]

I hope to hear from you soon. Thanks so much in advance,
-Mallory

## 3.1. Synthesis
The work of this phase brings together prior interviews, survey results against the background desk research’s sub-questions. It will require narrative transcription and visualisation of data.
Taking into account updates in the direction of the research output, the main question relevant to the synthesis stage is __. #todo

The synthesis stage also considers the interview data in the context of the following sub-questions from the desk research:
#todo

## 3.2. Preliminary analysis

The analysis of the entire endeavour begins. Ensuring that research questions have been properly answered, data is consistent and clear, and most of all that a compelling narrative for Infrared is above all clear, but also strategic, forward looking and mandated by Infrared’s mission statement.

## 3.3. Stakeholder feedback

Key senior leadership will help shape the analysis, while a feedback round for other key stakeholders, like those interviewed, will be conducted as a way to ensure the most salient points are properly emphasized, while also accounting for the incorporation of more visionary and fringe ideas. The stakeholder feedback phase creates the buy-in and dialogue necessary to promote the forthcoming external report as well as ensuring that the recommended applications are prioritised.

### 3.3.1. Other reviewers
- Michael Brennan, Ford
- Michuki, ISOC
- Dan Blah, Reset
- Others who have their research cited
- Anyone who was interviewed
- The rest of the Infrared
- [TBD]

### 3.3.2. Draft Invitation to review

#todo [Review invitation and instructions]
Live version to copy: [TBD]
We have some optional guidance questions for reviewers: 

1.	[TBD]

Thanks very much for your time! Looking forward to hearing from you,
-Mallory

### 3.3.3. Tracking review responses
#todo connect to project spreadsheet

## 3.4. External report publication

Finally two products will be shared. The first is a comprehensive, internal-only report on the research questions and recommended applications and next steps. We might share this with funders, too.

Secondarily an external report on independent infrastructure providers will serve the larger community in its research grounding, analysis and recommendations, with the ultimate goal of ensuring more and more effective… This is up to the Infrared members.
