Infrared: Informed Consent (for surveys and interviews)

[information about interviewer and relationship to infrared]

The purpose of this specific study is to gather a clear and in-depth understanding of Infrared members’ individual and collective needs and wishes and to clarify where Infrared can meet needs elsewhere by developing a map of the ecosystem it works within, constituted by several like-minded and often intersecting communities, other networks and coalitions.

This research will critically scope the current composition of Infrared, highlighting values alignment, needs and requirements that can enable action within the network for future shared projects. It will also map the landscape of digital human rights, internet freedom, and internet governance fora and coalitions where the Infrared network has external potential for collective action.

Research methods include desk research and summative evaluation of Infrared artefacts to date; quantitative survey analysis; qualitative interviews and analysis. You were selected because you are or have been a representative of an Infrared member or a notable affiliate to the network.

This project is funded by a grant from the Ford Foundation to Exchange Point Institute, a consulting nonprofit (501c3) that Mallory runs. During this project, multiple Infrared member representatives and affiliates will collaborate. As described below, however, only the core research team will have access to your personal information and research data. 

In this study, you will be asked a series of questions about the needs and challenges facing Infrared members. We will also ask you about your own experience in wider internet governance and related communities. The information you provide will then be aggregated and used to write a final report, for internal purposes only. We may use some direct quotations from your interview. In that case, we will not publish any identifying information in your quotations. 

With your permission, we will audio-record interviews and take detailed field notes while doing so. We will store these research artefacts-- surveys, recordings and transcripts, on secure Infrared services and servers that we administrate. Translation and transcription will be done with offline, local tools.

Audio recordings will be separated from any additional personal information, and permanently deleted after the report is complete.

Participation is completely voluntary, you can stop your participation anytime during the interview. We estimate your participation in the study will last up to one hour.

The risks and discomfort associated with participation in this study are no greater than those ordinarily encountered in daily life or during other online activities. If at any point you feel uncomfortable and would like to skip a question, please feel free to do so or to ask us to move to the next question. In the next 24 hours after the interview (and before we start transcribing your interview), you can still opt out of participation by emailing me. If you choose to withdraw from the study, we will delete your data and any information we have about your participation.

We hope you can participate. Please confirm in response to this email if you wish to proceed with the study interview. I will confirm your positive consent prior to the start of the interview. 

Thank you,
[Researcher]
[Email address]