Simple time tracker that discloses the overall amount of billable hours left in the project based on non-identifiable, aggregate figures taken from timesheet submissions. How many hours are left? Look to the bottom, right-most figure. Updated at the end of every month.

| 2023  | Jan-24  | Feb-24  | Mar-24  | Apr-24  | May-24  | Jun-24  | Jul-24  | Aug-24  | Sep-24  | Oct-24  | Nov-24  | Dec-24  |
|---|---|---|---|---|---|---|---|---|---|---|---|---|
| 15  | 5  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  | 0  |
| 385  | 380  | ---  | ---  | ---  | ---  | ---  | ---  | ---  | ---  | ---  | ---  | --- |
